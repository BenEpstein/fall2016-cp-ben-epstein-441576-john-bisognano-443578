var canvas = d3.select("body").append("svg")
    .attr("width", 1200)
    .attr("height", 600)

    .append("g") //whole tree stored in a "g" element
    .attr("transform", "translate(50,50)");
// var diagonal = d3.svg.diagonal()
// 	.source({x: 10, y: 10})
// 	.target({x: 300, y: 300});
var tree = d3.layout.tree() //funtion and object at same time, methods available to customize
    .size([400, 400]);

// .children(function(node){
//     return node.children;
// })
// .husband(function(node){
//     return node.husband;
// });

// d3.json("treedata.json", function (data){

var nodes = tree.nodes(data);
var links = tree.links(nodes); //array of objects with a source and a target (child node)


var node = canvas.selectAll(".node") //selecting all objects with class node (nothing right now)
    .data(nodes) 										//bind that to the data nodes
    .enter()
    .append("g")									 //adding it to our g element (entire body)
    .attr("class", "node")			 //giving it the class node
    .attr("transform", function (d) {
        return "translate(" + d.y + "," + d.x + ")"
    });


node.append("circle")
    .attr("r", 7)
    .style("fill", "lightsteelblue")
    .style("fill", "#fff")
    .style("stroke", "steelblue")
    .style("stroke-width", "3px")

    .on("click", function (d) {
        var nodeName = prompt("Edit name");
        d.name = nodeName;
        delete d["children"];
        update();
        d["children"] = new Array();
        console.log(data);

    });


node.append("text")
    .attr("x", function (d) {
        return d.children || d._children ? -13 : 13;
    })
    .attr("dy", ".35em")
    .attr("text-anchor", function (d) {
        return d.children || d._children ? "end" : "start";
    })
    .text(function (d) {
        return d.name;
    })

var diagonal = d3.svg.diagonal(); //function built into D3

canvas.selectAll(".link")
    .data(links)
    .enter()
    .append("path")
    .attr("class", "link")
    .attr("fill", "none")
    .attr("stroke", "#ADADAD")
    .attr("d", diagonal);
// .attr("d", elbow);
// })


//The following function was used from https://github.com/justincy specifically made for the horizontal lines in a family tree
var diagonal = d3.svg.diagonal() //function built into D3
    .projection(function (d) {
        return [d.y, d.x];
        // if(d.no_parent){
        //     return "M" + d.parent.y + "," + d.parent.x; //put pen down.
        // }
        // else{return [d.y, d.x];}
    });

// function elbow(d) { //Horizontal
//     return "M" + d.source.y + "," + d.source.x //put pen down.
//         + "H" + (d.source.y + (d.target.y - d.source.y) / 2) //Horizontal line to ...
//         + "V" + d.target.x                              //Vetical line to...
//         + "H" + d.target.y;                             //Horizontal line to...
// }

d3.select(self.frameElement).style("height", "500px");
var i = 0;
function update() {
    d3.select("svg").remove();
    var svg = d3.selectAll("body").append("svg")
        .attr("width", 1350)
        .attr("height", 600)
        .attr("position", "absolute")
        .append("g") //whole tree stored in a "g" element
        .attr("transform", "translate(50,50)");


    var tree = d3.layout.tree() //funtion and object at same time, methods available to customize
        .size([400, 1100]);
    nodes = tree.nodes(data);
    links = tree.links(nodes); //array of objects with a source and a target (child node)


    node = svg.selectAll(".node") //selecting all objects with class node (nothing right now)
        .data(nodes) 										//bind that to the data nodes
        .enter()
        .append("g")									 //adding it to our g element (entire body)
        .attr("class", "node")			 //giving it the class node
        .attr("transform", function (d) {
            return "translate(" + d.y + "," + d.x + ")"
        });


    node.append("circle")
        .attr("r", 7)
        .style("fill", "lightsteelblue")
        .style("fill", "#fff")
        .style("stroke", "steelblue")
        .style("stroke-width", "3px")
		.style("margin-left", "500px")
        // .attr("display", function(d){
        //     if(d.hidden){return "none"}
        //     else{return ""}
        // })
        .on("click", function (d) {
            var newName = prompt("Edit name or type d to delete person or type add to add a child");
			if(newName == "" || newName == null){
				
			}
            else if (newName == "add") {
                var childName = prompt("Enter child name");
                d.children = ( typeof d.children != 'undefined' && d.children instanceof Array ) ? d.children : [];
                d.children.push({"name": childName, "id": i});
                i++;
                console.log(data);
                update();
            }
            else if (newName === "d") {
                // if (d.parent.children.length === 1) {
                //     // window.location.replace("new%201.html");
                //     delete d.parent["children"];
                //     update();
                //     temp["children"] = new Array();
                // }
                // else {
                //     d.children = d.children.filter(function (el) {
                //         return el.id !== d.id;
                //     });
                //     console.log(data);
                //     update();
                // }
                // if (typeof d.children == 'undefined') {
                d.parent.children = d.parent.children.filter(function (el) {
                    return el.id !== d.id;
                });
                console.log(d.parent);
                if(d.parent.children.length === 0) {
                    update();
                    d.parent["children"] = new Array();
                }
                else{update();}

                // }
                // else{
                //
                // }
            }
            else {
                d.name = newName;
                if (typeof d.children == 'undefined') {
                    d["children"] = new Array();
                    update();
                }
                else if (d.children.length === 0) {
                    delete d["children"];
                    update();
                    d["children"] = new Array();
                }
                else {
                    update();
                }
            }
        });

//Text styling taken from bl.ocks.org D3.js tree
    node.append("text")
        .attr("x", function (d) {
            return d.children || d._children ? -13 : 13;
        })
		.style("fill", "#fff")
        .attr("dy", ".35em")
        .attr("text-anchor", function (d) {
            return d.children || d._children ? "end" : "start";
        })
        .text(function (d) {
            return d.name;
        })
    // .style("fill-opacity", 1e-6);


    svg.selectAll(".link")
        .data(links)
        .enter()
        .append("path")
        .attr("class", "link")
        .attr("fill", "none")
        .attr("stroke", "#ADADAD")
        .attr("d", diagonal);
    // .attr("d", elbow);

// ------The following was taken from D3 website (bl.ocks.org)-----------------------------------------------------------


}





