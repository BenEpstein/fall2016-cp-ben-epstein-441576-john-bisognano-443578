# README #

CSE 330: Creative Project
Ben Epstein 441576
John Bisognano	443578

### What is this project ###

In this creative project, we will be creating a species lineage tree that can be created by a user to the sight. 
The user can then save the tree to an account

### Grading? ###

Rubric:

### php laravel server is working correctly: 25pts ###
* Page renders correctly: 5pts
* Php connects and can send/receive data through mySQL: 10pts
* Inputs are added automatically: 10pts

### Tree is displayed using D3.js: 25pts ###
* D3 graph is visible with data: 10pts
* D3 is in tree shape and easy to read: 15pts
### User Management: 14pts ###
* A session is created when a user logs in: 3pts
* New users can register: 3pts
* Passwords are hashed and salted one-way encryption: 3pts
* Users can log out: 3pts
* Users can edit their tree: 2pts
### Usability: 8pts ###
* Site is intuitive and easy to navigate: 4pts
* Site is visually appealing: 4pts
### Best Practices: 9pts ###
* Site passes w3 validator: 2pts
* Code is well formatted and easy to read: 2pts
* Site is safe from SQL Injection attacks: 2pts
* Site follows the FIEO philosophy: 3pts
### Creative Portion: 14pts ###
