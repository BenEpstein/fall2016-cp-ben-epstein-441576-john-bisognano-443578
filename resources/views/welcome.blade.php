<!DOCTYPE html>
<meta charset="utf-8">
<style>

body {
background-image: url("http://wallpapercave.com/wp/MAmPaLt.jpg");
background-size: cover;
}
rect {
  fill: none;
  pointer-events: all;
}

.node {
  fill: #000;
}
.top-right {
   position: absolute;
   right: 10px;
   top: 18px;
 }

.cursor {
  fill: none;
  stroke: white;
  stroke-width: 5;
  pointer-events: none;
}

.link {
  stroke: red;
}
.links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

</style>
<body>
<h1 style="color:white;">Ben Epstein and John Bisognano</h1>
<h3 style="color:red">CSE 330: Creative Project</h3>
<p style="color:white">Laravel and D3.js</p>
<script src="//d3js.org/d3.v3.min.js"></script>
@if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div><br><br>
            @endif
<script>

var width = 1430,
    height = 660;


var fill = d3.scale.category20();

var force = d3.layout.force()
    .size([width, height])
    .nodes([{}]) // initialize with a single node
    .linkDistance(30)
    .charge(-60)
    .on("tick", tick);

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .on("mousemove", mousemove)
    .on("mousedown", mousedown);
	
var defs = svg.append('svg:defs');

defs.append("svg:pattern")
    .attr("id", "grump_avatar")
    .attr("width", 40)
    .attr("height", 40)
    .attr("patternUnits", "userSpaceOnUse")
    .append("svg:image")
    .attr("xlink:href", 'https://cse.wustl.edu/faculty/PublishingImages/Todd%20Sproull.JPG?RenditionID=4')
    .attr("width", 40)
    .attr("height", 40)
    .attr("x", 0)
    .attr("y", 0);
defs.append("svg:pattern")
    .attr("id", "brian")
    .attr("width", 500)
    .attr("height", 500)
    .attr("patternUnits", "userSpaceOnUse")
    .append("svg:image")
    .attr("xlink:href", 'https://scontent-dft4-2.xx.fbcdn.net/v/t1.0-9/14495336_1321449837866610_8099710996229218973_n.jpg?oh=e9626c1af07add052b1d1fffb1a209cd&oe=58F0FD90')
    .attr("width", 400)
    .attr("height", 400)
    .attr("x", -150)
    .attr("y", -110);

svg.append("rect")
    .attr("width", width)
    .attr("height", height);

var nodes = force.nodes(),
    links = force.links(),
    node = svg.selectAll(".node"),
    link = svg.selectAll(".link");

var cursor = svg.append("rect")
	.attr("width", 90)
	.attr("height", 90)
    .attr("transform", "translate(-100,-100)")
	.style("fill", "url(#brian)")
    .attr("class", "cursor");

restart();

function mousemove() {
  cursor.attr("transform", "translate(" + d3.mouse(this) + ")");
}

function mousedown() {
  var point = d3.mouse(this),
      node = {x: point[0], y: point[1]},
      n = nodes.push(node);

  // add links to any nearby nodes
  nodes.forEach(function(target) {
    var x = target.x - node.x,
        y = target.y - node.y;
    if (Math.sqrt(x * x + y * y) < 60) {
      links.push({source: node, target: target});
    }
  });

  restart();
}

function tick() {
  link.attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  node.attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; });
}

function restart() {
  link = link.data(links);

  link.enter().insert("line", ".node")
      .attr("class", "link");

  node = node.data(nodes);

  node.enter().insert("circle", ".cursor")
	  .style("fill", "url(#grump_avatar)")
      .attr("class", "node")
      .attr("r", 20)
      .call(force.drag);

  force.start();
}

</script>
</body>