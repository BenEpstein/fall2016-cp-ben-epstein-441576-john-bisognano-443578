@extends('layouts.app')

@section('content')
    <div class="createTitle">
        <!doctype html>
        <html>

        <head>
            <title> Family tree </title>
        </head>
        <body>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>


        <script>
            var i = 0;
            var data = {};
            data["name"] = "";
            console.log(data);
            function change(name) {
                $("#btnchange").text(name);
            }
        </script>

        <div id="tree">
            <form>
                <p id="instruction">First, enter your root animal</p>
                <input type="text" id="name" placeholder="Name">
                <input type="reset" class="button-xsmall pure-button" id="add" value="Add">
            </form>
            <script>
                var count = 0;
                $("#add").click(function () {
                    if (count == 0) {
                        $("#instruction").html("Now, add children");
                        data["name"] = $("#name").val();
                        update();
                        data["children"] = new Array();
                        count++;
                    }
                    else {
                        $("#instruction").html("Click on a specific node to edit or add more children.");
                        var value = $("#name").val();
                        data.children.push({"name": value, "id": i});
                        i++;
                        update();
                        console.log(data);
                    }

                });
            </script>
        </div>

        <div class="fb-share-button" data-href="http://localhost:8000/contact" data-layout="button_count"
             data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank"
                                                             href="">Share</a></div>

        </body>
        </html>

@stop
